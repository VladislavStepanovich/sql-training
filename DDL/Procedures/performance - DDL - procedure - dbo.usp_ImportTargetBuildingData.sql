use performance
go

if OBJECT_ID('dbo.usp_ImportBuildingsData', 'P') is not null
    drop procedure dbo.usp_ImportBuildingsData
go

create procedure dbo.usp_ImportBuildingsData
    @parameters nvarchar(max) 
as
begin
    set nocount on
    
    if @parameters is null
        raiserror('The value for @parameters should not be null!',15,1);

    create table #TargetBuilding
    (
        [Name]            nvarchar(20)     not null,
        City              nvarchar(20)     not null,
        Street            nvarchar(30)     not null,
        BuildingNumber    nvarchar(10)     not null,
        Halls             nvarchar(max)    not null,
    )

    create table #Hall
    (
        [Name]                       nvarchar(20)     not null,
        Capacity                     int              not null,
        [Type]                       nvarchar(50)     not null,
        Seats                        nvarchar(max)    null,  
        [Sessions]                   nvarchar(max)    not null,
        BuildingID                   int              not null,
        AvailablePerformanceTypes    nvarchar(max)    not null
    )

    create table #Seats
    (
        Places        nvarchar(max)    not null,
        [Row]         smallint         null,
        HallName      nvarchar(20)     not null,
        BuildingID    int              not null
    )

    create table #Place
    (
        [Row]         smallint        null,
        Place         smallint        not null,
        HallName      nvarchar(20)    not null,
        BuildingID    int             not null
    )

    create table #Session
    (
        [Time]        time(3)         not null,
        [Name]        nvarchar(20)    not null,
        BuildingID    int             not null
    )

    create table #HallTypePerformanceType
    (
        PerformanceType    nvarchar(20)    not null,
        HallType           nvarchar(20)    not null
    )

    create table #HallHallType
    (
        [Type]        nvarchar(50)    not null,
        [Name]        nvarchar(20)    not null,
        BuildingID    int             not null
    )

    insert into #TargetBuilding
    (
        [Name],
        City,              
        Street,          
        BuildingNumber,   
        Halls
    )
    select 
        tb.[Name],
        tb.City,              
        tb.Street,          
        tb.BuildingNumber,
        tb.Halls
    from openjson(@parameters)
        with
        (
            [Name]            nvarchar(30)     N'$.name',
            City              nvarchar(20)     N'$.address.city',
            Street            nvarchar(30)     N'$.address.street',
            BuildingNumber    nvarchar(10)     N'$.address.buildingNumber',
            Halls             nvarchar(max)    N'$.halls'                      as json
        ) tb
    
    --dbo.TargetBuilding
    insert into dbo.TargetBuilding
    (
        [Name],
        City,
        Street,
        BuildingNumber
    )
    select distinct
        [Name],
        City,
        Street,
        BuildingNumber
    from #TargetBuilding

    insert into #Hall
    (
        [Name],
        Capacity,
        [Type],
        Seats,
        [Sessions],
        BuildingID,
        AvailablePerformanceTypes
    )
    select 
        h.[Name],
        h.Capacity,
        h.[Type],
        h.Seats,
        h.[Sessions],
        tb.TargetBuildingID,
        h.AvailablePerformanceTypes
    from #TargetBuilding temptb
    join dbo.TargetBuilding tb
        on tb.City = temptb.City
            and tb.[Name] = temptb.[Name]
    cross apply openjson(temptb.Halls)
        with
        (
            [Name]                       nvarchar(20)     N'$.name',
            Capacity                     int              N'$.capacity',
            [Type]                       nvarchar(50)     N'$.type',
            Seats                        nvarchar(max)    N'$.seats'                        as json,
            [Sessions]                   nvarchar(max)    N'$.sessions'                     as json,
            AvailablePerformanceTypes    nvarchar(max)    N'$.availablePerformanceTypes'    as json
        ) h

    insert into #HallHallType
    (
        [Type],
        [Name],
        BuildingID
    )
    select distinct
        ht.[Type],
        h.[Name],
        h.BuildingID
    from #Hall h
    join dbo.HallType ht
        on h.[Type] = ht.[Type]

    insert into #HallTypePerformanceType
    (
        HallType,
        PerformanceType
    )
    select distinct
        h.[Type],
        t.[Type]
    from #Hall h
    cross apply openjson(h.AvailablePerformanceTypes)
        with
        (
            [Type]    nvarchar(50)    N'$.type'
        ) t
        
    insert into #Seats
    (
        Places,
        [Row],
        HallName,
        BuildingID
    )
    select 
        s.Places,
        s.[Row],
        h.[Name],
        h.BuildingID
    from #Hall h
    cross apply openjson(h.Seats)
        with
        (
            Places    nvarchar(max)    N'$.places'    as json,
            [Row]     int              N'$.row'
        ) s
        
    insert into #Session
    (
        [Time],
        [Name],
        BuildingID
    )
    select 
        s.[Time],
        h.[Name],
        h.BuildingID
    from #Hall h
    cross apply openjson(h.[Sessions])
        with
        (
            [Time]    time(3)    N'$.time'
        ) s
        
    insert into #Place
    (
        [Row],
        Place,
        HallName,
        BuildingID
    )
    select 
        s.[Row],
        a.value,
        s.HallName,
        s.BuildingID
    from #Seats s
    cross apply openjson(s.Places) a

    --dbo.Hall
    insert into dbo.Hall
    (
        TargetBuildingID,
        [Name],
        Capacity
    )
    select distinct
        BuildingID,
        [Name],
        Capacity
    from #Hall 
    
    --dbo.HallType
    insert into dbo.HallType
    (
        [Type]
    )
    select distinct
        [Type]
    from #Hall

    --[dbo].[Session]
    insert into [dbo].[Session]
    (
        HallID,
        [Time]
    )
    select distinct
        HallID,
        [Time]
    from dbo.Hall h 
    join #Session s 
        on s.[Name] = h.[Name]
            and s.BuildingID = h.TargetBuildingID
    
    --dbo.Seat
    insert into dbo.Seat
    (
        HallID,
        [Row],
        Place
    )
    select distinct
        h.HallID,
        p.[Row],
        p.Place
    from dbo.Hall h
    join #Place p 
        on p.HallName = h.[Name]
            and p.BuildingID = h.TargetBuildingID

    --dbo.HallHallType
    insert into dbo.HallHallType
    (
        HallID, 
        HallTypeID
    )
    select distinct
        h.HallID,
        ht.HallTypeID
    from #HallHallType hht
    join dbo.Hall h 
        on hht.[Name] = h.[Name]
            and hht.BuildingID = h.TargetBuildingID
    join dbo.HallType ht
        on hht.[Type] = ht.[Type]

    --dbo.HallTypePerformanceType
    insert into dbo.HallTypePerformanceType
    (
        HallTypeID, 
        PerformanceTypeID
    )
    select distinct
        ht.HallTypeID,
        pt.PerformanceTypeID
    from #HallTypePerformanceType htpt
    join dbo.HallType ht 
        on htpt.HallType = ht.[Type]
    join dbo.PerformanceType pt
        on htpt.PerformanceType = pt.[Type]
end
