use performance
go

if OBJECT_ID('dbo.usp_ImportPurchasesData', 'P') is not null
    drop procedure dbo.usp_ImportPurchasesData
go

create procedure dbo.usp_ImportPurchasesData
    @parameters nvarchar(max)
as
begin
    set nocount on

    if @parameters is null
        raiserror('The value for @parameters should not be null!',15,1);

    create table #Purchase
    (
        PerformanceTitle    nvarchar(50)    not null,
        PerformanceType     nvarchar(20)    not null,
        [Language]          nvarchar(20)    not null,
        Building            nvarchar(20)    not null,
        Hall                nvarchar(20)    not null,
        City                nvarchar(20)    not null,
        [Date]              date            not null,
        [Time]              time            not null,
        [Row]               smallint        null,
        Place               smallint        not null,
        Price               money           not null,
        FirstName           nvarchar(50)    not null,
        LastName            nvarchar(50)    not null,
        MiddleName          nvarchar(50)    null,
        Birthday            date            not null
    )

    insert into #Purchase(
        PerformanceTitle,
        PerformanceType,
        [Language],
        Building,
        Hall,
        City,
        [Date],
        [Time],
        [Row],
        Place,
        Price,
        FirstName,
        LastName,
        MiddleName,
        Birthday
    )
    select 
        PerformanceTitle,
        PerformanceType,
        [Language],
        Building,
        Hall,
        City,
        cast([Date] as date),
        cast([Time] as time),
        [Row],
        Place,
        Price,
        FirstName,
        LastName,
        MiddleName,
        cast(Birthday as date)
    from openjson(@parameters)
        with
        (
            PerformanceTitle    nvarchar(30)    N'$.Ticket.performance',
            PerformanceType     nvarchar(20)    N'$.Ticket.performanceType',
            [Language]          nvarchar(20)    N'$.Ticket.language',
            Building            nvarchar(20)    N'$.Ticket.building',
            City                nvarchar(20)    N'$.Ticket.city',
            Hall                nvarchar(20)    N'$.Ticket.hall',
            [Date]              date            N'$.Ticket.date',
            [Time]              time            N'$.Ticket.time',
            [Row]               smallint        N'$.Ticket.seat.row',
            Place               smallint        N'$.Ticket.seat.place',
            Price               money           N'$.Ticket.price',
            FirstName           nvarchar(50)    N'$.Person.firstName',
            LastName            nvarchar(50)    N'$.Person.lastName',
            MiddleName          nvarchar(50)    N'$.Person.middleName',
            Birthday            date            N'$.Person.birthday'
        ) prch
   
    --dbo.Purchase
    insert into dbo.Purchase
    (
        TicketID,
        PersonID
    )
    select distinct
        TicketID,
        p.PersonID
    from #Purchase prch
    join dbo.Person p
        on prch.Birthday = p.Birthday
            and prch.FirstName = p.FirstName
            and prch.LastName = p.LastName
            and ((prch.MiddleName = p.MiddleName) 
                or (prch.MiddleName is null and p.MiddleName is null))
    join [dbo].[Language] l
        on l.[Name] = prch.[Language]
    join dbo.PerformanceType pt
        on pt.[Type] = prch.PerformanceType
    join dbo.Performance prf 
        on prf.Title = prch.PerformanceTitle
            and prf.LanguageID = l.LanguageID
            and prf.PerformanceTypeID = pt.PerformanceTypeID
    join dbo.TargetBuilding tb 
        on prch.Building = tb.[Name]
            and prch.City = tb.City
    join dbo.Hall h
        on prch.Hall = h.[Name]
            and tb.TargetBuildingID = h.TargetBuildingID
    join dbo.[Session] ss
        on ss.HallID = h.HallID
            and prch.[Time] = ss.[Time]
    join dbo.Seat s 
        on h.HallID = s.HallID
            and prch.Place = s.Place
            and ((prch.[Row] is null and s.[Row] is null) or (prch.[Row] =  s.[Row]))
    join dbo.Ticket t
        on prch.[Date] = t.[Date]
            and s.SeatID = t.SeatID
            and prf.PerformanceID = t.PerformanceID
            and ss.SessionID = t.SessionID
end
