use performance
go

if OBJECT_ID('dbo.usp_ImportPerformanceData', 'P') is not null
    drop procedure dbo.usp_ImportPerformanceData
go

create procedure dbo.usp_ImportPerformanceData
    @parameters nvarchar(max)
as
begin
    set nocount on
    
    if @parameters is null
        raiserror('The value for @parameters should not be null!',15,1);

    create table #Performance
    (
        PerformanceType      nvarchar(20)      not null,
        Title                nvarchar(50)      not null,
        Duration             time(3)           not null,
        [Description]        nvarchar(2000)    not null,
        Category             nvarchar(max)     not null,
        [Language]           nvarchar(20)      not null,
        Participant          nvarchar(max)     not null
    )

    create table #PerformanceCategory
    (
        Title              nvarchar(50)    not null,
        PerformanceType    nvarchar(20)    not null,
        [Language]         nvarchar(20)    not null,
        Category           nvarchar(30)    not null,
    )

    create table #Participant
    (
        Title              nvarchar(50)     not null,
        PerformanceType    nvarchar(20)     not null,
        [Language]         nvarchar(20)     not null,
        FirstName          nvarchar(50)     not null,
        LastName           nvarchar(50)     not null,
        MiddleName         nvarchar(50)     null,
        Birthday           date             not null,
        ParticipantType    nvarchar(max)    not null
    )

    create table #PersonParticipantType
    (
        Title              nvarchar(50)    not null,
        PerformanceType    nvarchar(20)    not null,
        [Language]         nvarchar(20)    not null,
        FirstName          nvarchar(50)    not null,
        LastName           nvarchar(50)    not null,
        MiddleName         nvarchar(50)    null,
        Birthday           date            not null,
        ParticipantType    nvarchar(20)    not null
    )

    insert into #Performance(
        PerformanceType,
        Title,
        [Language],
        Duration,
        [Description],
        Category,
        Participant
    )
    select 
        PerformanceType,
        Title,
        [Language],
        cast(Duration as time),
        [Description],
        Category,
        Participant
    from openjson(@parameters)
        with
        (
            PerformanceType      nvarchar(20)      N'$.performanceType',
            Title                nvarchar(50)      N'$.title',
            [Language]           nvarchar(20)      N'$.language',
            Duration             time(3)           N'$.duration',        
            [Description]        nvarchar(2000)    N'$.description',
            Category             nvarchar(max)     N'$.category'          as json,
            Participant          nvarchar(max)     N'$.participants'      as json
        ) pf
    
    insert into #PerformanceCategory
    (
        Title,
        PerformanceType,
        [Language],
        Category
    )
    select
        p.Title,
        p.PerformanceType,
        p.[Language],
        c.Category
    from #Performance p
    cross apply openjson(p.Category)
        with
        (
            Category    nvarchar(30)   N'$.name'   
        ) c

    insert into #Participant(
        Title,
        PerformanceType,
        [Language],
        FirstName,
        LastName,
        MiddleName,
        Birthday,
        ParticipantType
    )
    select 
        p.Title,
        p.PerformanceType,
        p.[Language],
        pp.FirstName,
        pp.LastName,
        pp.MiddleName,
        cast(pp.Birthday as date),
        pp.ParticipantType
    from #Performance p
    cross apply openjson(p.Participant)
        with
        (
           FirstName          nvarchar(50)     N'$.firstName',
           LastName           nvarchar(50)     N'$.lastName',
           MiddleName         nvarchar(50)     N'$.middleName',
           Birthday           date             N'$.birthday',
           ParticipantType    nvarchar(max)    N'$.participantType' as json
        ) pp
     
     --dbo.PerformanceType
     insert into dbo.PerformanceType
     (
        [Type]
     )
     select distinct pt.PerformanceType
     from #Performance pt
    
     --dbo.ParticipantType
     insert into dbo.ParticipantType
     (
        [Type]
     )
     select distinct pt.[Type]
     from #Participant p
     cross apply openjson(p.ParticipantType)
         with
         (
             [Type]    nvarchar(20)    N'$.type'
         ) pt

     --dbo.Category
     insert into dbo.Category
     (
         [Name]
     )
     select distinct pc.[Name]
     from #Performance p
     cross apply openjson(p.Category)
         with
         (
             [Name]    nvarchar(30)    N'$.name'
         ) pc

     --dbo.Language
     insert into [dbo].[Language]
     (
         [Name]
     )
     select distinct p.[Language]
     from #Performance p
    
    --dbo.Person
    insert into dbo.Person
    (
        FirstName,
        LastName,
        MiddleName,
        Birthday,
        [Login],
        [Password]
     )
    select
        prt.FirstName,
        prt.LastName,
        prt.MiddleName,
        prt.Birthday,
        prs.[Login],
        prs.[Password]
     from #Participant prt 
     left join dbo.Person prs 
         on prt.FirstName = prs.FirstName 
             and prt.LastName = prs.LastName 
             and prt.MiddleName = prs.MiddleName 
             and prt.Birthday = prs.Birthday;

    insert into #PersonParticipantType
    (
        Title,
        PerformanceType,
        [Language],
        FirstName,
        LastName,
        MiddleName,
        Birthday,
        ParticipantType
    )
    select
        p.Title,
        p.PerformanceType,
        p.[Language],
        p.FirstName,
        p.LastName,
        p.MiddleName,
        p.Birthday,
        pt.[Type]
     from #Participant p 
     cross apply openjson(p.ParticipantType)
         with
         (
             [Type]    nvarchar(20)    N'$.type'
         ) pt

    --dbo.Participant 
    insert into dbo.Participant
    (
        PersonID
    )
    select PersonID from dbo.Person
    where [Login] is null AND [Password] is null

    --dbo.Performance
    insert into dbo.Performance
    (
        PerformanceTypeID,
        Title,
        LanguageID,
        Duration,
        [Description]
    )
    select 
        pt.PerformanceTypeID,
        p.Title,
        l.LanguageID,
        p.Duration,
        p.[Description]
    from #Performance p 
    join dbo.PerformanceType pt 
        on p.PerformanceType = pt.[Type]
    join [dbo].[Language] l
        on p.[Language] = l.[Name]
          
    --dbo.PerformanceCategory
    insert into dbo.PerformanceCategory
    (
        PerformanceID,
        CategoryID
    )
    select
        p.PerformanceID,
        c.CategoryID
    from #PerformanceCategory pc
    join [dbo].[Language] l
        on pc.[Language] = l.[Name]
    join dbo.Category c 
        on pc.Category = c.[Name]
	join dbo.PerformanceType pt
	    on pc.PerformanceType = pt.[Type]
    join dbo.Performance p 
        on pc.Title = p.Title
            and l.LanguageID = p.LanguageID
            and pt.PerformanceTypeID = p.PerformanceTypeID

    --dbo.PerformanceParticipantType 
    insert into PerformanceParticipantType
    (
        PerformanceID,
        ParticipantID,
        ParticipantTypeID
    )
    select
        p.PerformanceID,
        pt.ParticipantID,
        ptt.ParticipantTypeID
    from #PersonParticipantType ppt 
    join [dbo].[Language] l
        on ppt.[Language] = l.[Name]
	join dbo.PerformanceType prft
	    on ppt.PerformanceType = prft.[Type]
    join dbo.Performance p 
        on ppt.Title = p.Title
            and l.LanguageID = p.LanguageID
            and prft.PerformanceTypeID = p.PerformanceTypeID
    join dbo.Person pr 
        on ppt.FirstName = pr.FirstName 
            and ppt.LastName = pr.LastName 
            and ppt.MiddleName = pr.MiddleName 
                or (ppt.MiddleName is null and pr.MiddleName is null)
            and ppt.Birthday = pr.Birthday
    join Participant pt 
        on pt.PersonID = pr.PersonID
    join ParticipantType ptt 
        on ptt.[Type] = ppt.ParticipantType
end
