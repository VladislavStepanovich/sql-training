use performance
go

if OBJECT_ID('dbo.usp_ImportPersonData', 'P') is not null
    drop procedure dbo.usp_ImportPersonData
go

create procedure dbo.usp_ImportPersonData
    @parameters nvarchar(max)
as
begin
    set nocount on

	if @parameters is null
	    raiserror('The value for @parameters should not be null!',15,1);

    create table #Person
    (
        FirstName     nvarchar(50)    not null,
        LastName      nvarchar(50)    not null,
        MiddleName    nvarchar(50)    null,
        Birthday      date            not null,
        [Login]       nvarchar(30)    null,
        [Password]    nvarchar(30)    null,
    )

    insert into #Person(
        FirstName,
        LastName,
        MiddleName,
        Birthday,
        [Login],
        [Password]
    )
    select 
        jc.FirstName,
        jc.LastName,
        jc.MiddleName,
        cast(jc.Birthday as date),
        jc.[Login],
        jc.[Password]
    from openjson(@parameters)
        with
        (
            FirstName     nvarchar(50)     N'$.firstName',
            LastName      nvarchar(50)     N'$.lastName',
            MiddleName    nvarchar(50)     N'$.middleName',
            Birthday      date             N'$.birthday',
            [Login]       nvarchar(30)     N'$.login',
            [Password]    nvarchar(30)     N'$.password'
        ) jc

    --dbo.Person
    insert into dbo.Person
    (
         FirstName,
         LastName,
         MiddleName,
         Birthday,
         [Login],
         [Password]
    )
    select 
         jc.FirstName,
         jc.LastName,
         jc.MiddleName,
         jc.Birthday,
         jc.[Login],
         jc.[Password]
    from #Person jc

end







