use performance
go

if OBJECT_ID('dbo.usp_ImportTicketData', 'P') is not null
    drop procedure dbo.usp_ImportTicketData
go

create procedure dbo.usp_ImportTicketData
    @parameters nvarchar(max)
as
begin
    set nocount on

    if @parameters is null
        raiserror('The value for @parameters should not be null!',15,1);

    create table #Ticket
    (
        PerformanceTitle    nvarchar(50)    not null,
        PerformanceType     nvarchar(20)    not null,
        [Language]          nvarchar(20)    not null,
        Building            nvarchar(20)    not null,
        City                nvarchar(20)    not null,
        Hall                nvarchar(20)    not null,
        [Date]              date            not null,
        [Time]              time            not null,
        [Row]               smallint        null,
        Place               smallint        not null,
        Price               money           not null
    )
    
    insert into #Ticket
    (
        PerformanceTitle,
        PerformanceType,
        [Language],
        Building,
        Hall,
        [Date],
        [Time],
        [Row],
        Place,
        City,
        Price
    )
    select 
        PerformanceTitle,
        PerformanceType,
        [Language],
        Building,
        Hall,
        [Date],
        [Time],
        [Row],
        Place,
        City,
        Price
    from openjson(@parameters)
        with
        (
            PerformanceTitle    nvarchar(30)    N'$.performance',
            PerformanceType     nvarchar(20)    N'$.performanceType',
            [Language]          nvarchar(20)    N'$.language',
            Building            nvarchar(20)    N'$.building',
            Hall                nvarchar(20)    N'$.hall',
            [Date]              date            N'$.date',
            [Time]              time            N'$.time',
            [Row]               smallint        N'$.seat.row',
            Place               smallint        N'$.seat.place',
            City                nvarchar(20)    N'$.city',
            Price               money           N'$.price'
        ) t

    --dbo.Ticket
    insert into dbo.Ticket(
        SeatID,
        PerformanceID,
        SessionID,
        [Date],
        Price
    )
    select distinct
        s.SeatID,
        prf.PerformanceID,
        ss.SessionID,
        t.[Date],
        t.Price
    from #Ticket t
    join [dbo].[Language] l
        on l.[Name] = t.[Language]
    join dbo.PerformanceType pt
        on pt.[Type] = t.PerformanceType
    join dbo.Performance prf 
        on prf.Title = t.PerformanceTitle
            and prf.LanguageID = l.LanguageID
            and prf.PerformanceTypeID = pt.PerformanceTypeID
    join dbo.TargetBuilding  tb 
        on t.Building = tb.[Name]
            and t.City = tb.City
    join dbo.Hall h
        on t.Hall = h.[Name]
            and h.TargetBuildingID = tb.TargetBuildingID
    join dbo.[Session] ss
        on ss.HallID = h.HallID
            and t.[Time] = ss.[Time]
    join dbo.Seat s 
        on h.HallID = s.HallID
            and t.Place = s.Place
            and ((t.[Row] is null and s.[Row] is null) or (t.[Row] =  s.[Row]))

end