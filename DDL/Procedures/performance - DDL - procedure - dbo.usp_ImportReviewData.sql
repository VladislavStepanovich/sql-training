use performance
go

if OBJECT_ID('dbo.usp_ImportReviewData', 'P') is not null
    drop procedure dbo.usp_ImportReviewData
go

create procedure dbo.usp_ImportReviewData
    @parameters nvarchar(max)
as
begin
    set nocount on
    
    if @parameters is null
        raiserror('The value for @parameters should not be null!',15,1);

    create table #Review
    (
        PerformanceTitle     nvarchar(50)     not null,
        PerformanceType      nvarchar(20)     not null,
        [Language]           nvarchar(20)     not null,
        Rating               float            null,
        Comment              nvarchar(500)    null,
        [Datetime]           datetime         not null,
        FirstName            nvarchar(50)     not null,
        LastName             nvarchar(50)     not null,
        MiddleName           nvarchar(50)     null,        
        Birthday             date             not null
    )

    insert into #Review
    (
        PerformanceTitle,
        PerformanceType,
        [Language],
        Rating,
        Comment,
        [Datetime],
        FirstName,
        LastName,
        MiddleName,
        Birthday
    )
    select
        PerformanceTitle,
        PerformanceType,
        [Language],
        cast(Rating as float),
        Comment,
        convert(datetime, [Datetime], 120),
        FirstName,
        LastName,
        MiddleName,
        Birthday
    from openjson(@parameters)
        with
        (
            PerformanceTitle      nvarchar(50)     N'$.performance',
            PerformanceType       nvarchar(20)     N'$.performanceType',
            [Language]            nvarchar(20)     N'$.language',
            Rating                float            N'$.rating',
            Comment               nvarchar(500)    N'$.comment',         
            [Datetime]            datetime         N'$.datetime',
            FirstName             nvarchar(50)     N'$.person.firstName',
            LastName              nvarchar(50)     N'$.person.lastName',
            MiddleName            nvarchar(50)     N'$.person.middleName',        
            Birthday              date             N'$.person.birthday'
        ) rv

    --dbo.Comment
    insert into dbo.Comment
    (
        PersonID,
        PerformanceID,
        [Datetime],
        Content
    ) 
    select
        p.PersonID,
        prf.PerformanceID,
        rv.[Datetime],
        rv.Comment
    from #Review rv 
    join dbo.Person p
        on rv.FirstName = p.FirstName
            and rv.LastName = p.LastName
            and rv.MiddleName = p.MiddleName 
                 or (rv.MiddleName = null and p.MiddleName = null)
            and rv.Birthday = p.Birthday
    join [dbo].[Language] l
        on l.[Name] = rv.[Language]
    join dbo.PerformanceType pt
        on pt.[Type] = rv.PerformanceType
    join dbo.Performance prf 
        on prf.Title = rv.PerformanceTitle
            and prf.LanguageID = l.LanguageID
            and prf.PerformanceTypeID = pt.PerformanceTypeID

    --dbo.Rating
    insert into dbo.Rating
    (
        PersonID,
        PerformanceID,
        Rate
    ) 
    select
        p.PersonID,
        prf.PerformanceID,
        rv.Rating
    from #Review rv 
    join dbo.Person p
        on rv.FirstName = p.FirstName
            and rv.LastName = p.LastName
            and rv.MiddleName = p.MiddleName 
                 or (rv.MiddleName = null and p.MiddleName = null)
            and rv.Birthday = p.Birthday
    join [dbo].[Language] l
        on l.[Name] = rv.[Language]
    join dbo.PerformanceType pt
        on pt.[Type] = rv.PerformanceType
    join dbo.Performance prf 
        on prf.Title = rv.PerformanceTitle
            and prf.LanguageID = l.LanguageID
            and prf.PerformanceTypeID = pt.PerformanceTypeID
    where rv.Rating is not null;
end