use performance
go

--AK_HallType_Type
if OBJECT_ID ('AK_HallType_Type', 'UQ') is not null
    alter table dbo.HallType drop constraint AK_HallType_Type
go

alter table dbo.HallType
    add constraint AK_HallType_Type unique
    (
        [Type]
    )
go