use performance
go

--AK_Session_HallID_Time
if OBJECT_ID ('AK_Session_HallID_Time', 'UQ') is not null
    alter table [dbo].[Session] drop constraint AK_Session_HallID_Time
go

alter table [dbo].[Session]
    add constraint AK_Session_HallID_Time unique
    (
        HallID,
        [Time]
    )
go

--FK_Session_Hall
if OBJECT_ID('FK_Session_Hall', 'F') is not null
    alter table [dbo].[Session] drop constraint FK_Session_Hall
go

alter table [dbo].[Session]
    add constraint FK_Session_Hall foreign key (HallID) references dbo.Hall (HallID)
go