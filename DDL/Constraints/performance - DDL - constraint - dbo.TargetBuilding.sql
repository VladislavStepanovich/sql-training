use performance
go

--AK_TargetBuilding_Name_City
if OBJECT_ID ('AK_TargetBuilding_Name_City', 'UQ') is not null
    alter table dbo.TargetBuilding drop constraint AK_TargetBuilding_Name_City
go

alter table dbo.TargetBuilding
    add constraint AK_TargetBuilding_Name_City unique
    (
        [Name],
        City
    )
go