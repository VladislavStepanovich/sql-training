use performance
go

--AK_Category_Name
if OBJECT_ID ('AK_Category_Name', 'UQ') is not null
    alter table dbo.Category drop constraint AK_Category_Name
go

alter table dbo.Category
    add constraint AK_Category_Name unique
    (
        [Name]
    )
go
