use performance
go

--AK_Person_FirstName_LastName_MiddleName_Birthday
if OBJECT_ID ('AK_Person_FirstName_LastName_MiddleName_Birthday', 'UQ') is not null
    alter table dbo.Person drop constraint AK_Person_FirstName_LastName_MiddleName_Birthday
go

alter table dbo.Person
    add constraint AK_Person_FirstName_LastName_MiddleName_Birthday unique
    (
        FirstName,
        LastName,
        MiddleName,
        Birthday
    )
go




