use performance
go

--FK_Participant_Person
if OBJECT_ID('FK_Participant_Person', 'F') is not null
    alter table dbo.Participant drop constraint FK_Participant_Person
go

alter table dbo.Participant
    add constraint FK_Participant_Person foreign key (PersonID) references dbo.Person (PersonID)
go