use performance
go

--FK_Ticket_Seat
if OBJECT_ID('FK_Ticket_Seat', 'F') is not null
    alter table dbo.Ticket drop constraint FK_Ticket_Seat
go

alter table dbo.Ticket
    add constraint FK_Ticket_Seat foreign key (SeatID) references dbo.Seat (SeatID)
go

--FK_Ticket_Performance
if OBJECT_ID('FK_Ticket_Performance', 'F') is not null
    alter table dbo.Ticket drop constraint FK_Ticket_Performance
go

alter table dbo.Ticket
    add constraint FK_Ticket_Performance foreign key (PerformanceID) references dbo.Performance (PerformanceID)
go

--FK_Ticket_Session
if OBJECT_ID('FK_Ticket_Session', 'F') is not null
    alter table dbo.Ticket drop constraint FK_Ticket_Session
go

alter table dbo.Ticket
    add constraint FK_Ticket_Session foreign key (SessionID) references dbo.[Session] (SessionID)
go

--AK_Ticket_PerformanceID_SessionID_Date
if OBJECT_ID ('AK_Ticket_SeatID_PerformanceID_SessionID_Date', 'UQ') is not null
    alter table dbo.Ticket drop constraint AK_Ticket_SeatID_PerformanceID_SessionID_Date
go

alter table dbo.Ticket
    add constraint AK_Ticket_SeatID_PerformanceID_SessionID_Date unique
    (
	    SeatID,
        PerformanceID,
		SessionID,
		[Date]
    )
go