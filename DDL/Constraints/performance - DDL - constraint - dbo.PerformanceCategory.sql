use performance
go

--FK_PerformanceCategory_Category
if OBJECT_ID('FK_PerformanceCategory_Category', 'F') is not null
    alter table dbo.PerformanceCategory drop constraint FK_PerformanceCategory_Category
go

alter table dbo.PerformanceCategory
    add constraint FK_PerformanceCategory_Category foreign key (CategoryID) references dbo.Category (CategoryID)
go

--FK_PerformanceCategory_Performance
if OBJECT_ID('FK_PerformanceCategory_Performance', 'F') is not null
    alter table dbo.PerformanceCategory drop constraint FK_PerformanceCategory_Performance
go

alter table dbo.PerformanceCategory
    add constraint FK_PerformanceCategory_Performance foreign key (PerformanceID) references dbo.Performance (PerformanceID)
go
