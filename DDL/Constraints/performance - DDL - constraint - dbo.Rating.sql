use performance
go

--FK_Rating_Person
if OBJECT_ID('FK_Rating_Person', 'F') is not null
    alter table dbo.Rating drop constraint FK_Rating_Person
go

alter table dbo.Rating
    add constraint FK_Rating_Person foreign key (PersonID) references dbo.Person (PersonID)
go

--FK_Rating_Performance
if OBJECT_ID('FK_Rating_Performance', 'F') is not null
    alter table dbo.Rating drop constraint FK_Rating_Performance
go

alter table dbo.Rating
    add constraint FK_Rating_Performance foreign key (PerformanceID) references dbo.Performance (PerformanceID)
go

--AK_Rating_PersonID_PerformanceID_Rate
if OBJECT_ID ('AK_Rating_PersonID_PerformanceID_Rate', 'UQ') is not null
    alter table dbo.Rating drop constraint AK_Rating_PersonID_PerformanceID_Rate
go

alter table dbo.Rating
    add constraint AK_Rating_PersonID_PerformanceID_Rate unique
    (
        PersonID,
        PerformanceID,
		Rate
    )
go