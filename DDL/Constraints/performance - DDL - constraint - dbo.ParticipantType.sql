use performance
go

--AK_ParticipantType_Type
if OBJECT_ID ('AK_ParticipantType_Type', 'UQ') is not null
    alter table dbo.ParticipantType drop constraint AK_ParticipantType_Type
go

alter table dbo.ParticipantType
    add constraint AK_ParticipantType_Type unique
    (
        [Type]
    )
go