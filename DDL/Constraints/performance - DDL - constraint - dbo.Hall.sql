use performance
go

--AK_Hall_TargetBuildingID_Name
if OBJECT_ID ('AK_Hall_TargetBuildingID_Name', 'UQ') is not null
    alter table dbo.Hall drop constraint AK_Hall_TargetBuildingID_Name
go

alter table dbo.Hall
    add constraint AK_Hall_TargetBuildingID_Name unique
    (
	    TargetBuildingID,
        [Name]
    )
go

--FK_Hall_TargetBuilding
if OBJECT_ID('FK_Hall_TargetBuilding', 'F') is not null
    alter table dbo.Hall drop constraint FK_Hall_TargetBuilding
go

alter table dbo.Hall
    add constraint FK_Hall_TargetBuilding foreign key (TargetBuildingID) references dbo.TargetBuilding (TargetBuildingID)
go