use performance
go

--FK_PerformanceParticipantType_ParticipantType
if OBJECT_ID('FK_PerformanceParticipantType_ParticipantType', 'F') is not null
    alter table dbo.PerformanceParticipantType drop constraint FK_PerformanceParticipantType_ParticipantType
go

alter table dbo.PerformanceParticipantType
    add constraint FK_PerformanceParticipantType_ParticipantType foreign key (ParticipantTypeID) references dbo.ParticipantType (ParticipantTypeID)
go

--FK_PerformanceParticipantType_Participant
if OBJECT_ID('FK_PerformanceParticipantType_Participant', 'F') is not null
    alter table dbo.PerformanceParticipantType drop constraint FK_PerformanceParticipantType_Participant
go

alter table dbo.PerformanceParticipantType
    add constraint FK_PerformanceParticipantType_Participant foreign key (ParticipantID) references dbo.Participant (ParticipantID)
go

--FK_PerformanceParticipantType_Performance
if OBJECT_ID('FK_PerformanceParticipantType_Performance', 'F') is not null
    alter table dbo.PerformanceParticipantType drop constraint FK_PerformanceParticipantType_Performance
go

alter table dbo.PerformanceParticipantType
    add constraint FK_PerformanceParticipantType_Performance foreign key (PerformanceID) references dbo.Performance (PerformanceID)
go