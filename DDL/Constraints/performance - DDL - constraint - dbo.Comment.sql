use performance
go

--FK_Comment_Person
if OBJECT_ID('FK_Comment_Person', 'F') is not null
    alter table dbo.Comment drop constraint FK_Comment_Person
go

alter table dbo.Comment
    add constraint FK_Comment_Person foreign key (PersonID) references dbo.Person (PersonID)
go

--FK_Comment_Performance
if OBJECT_ID('FK_Comment_Performance', 'F') is not null
    alter table dbo.Comment drop constraint FK_Comment_Performance
go

alter table dbo.Comment
    add constraint FK_Comment_Performance foreign key (PerformanceID) references dbo.Performance (PerformanceID)
go

--AK_Comment_PersonID_PerformanceID_Content_Datetime
if OBJECT_ID ('AK_Comment_PersonID_PerformanceID_Content_Datetime', 'UQ') is not null
    alter table dbo.Comment drop constraint AK_Comment_PersonID_PerformanceID_Content_Datetime
go

alter table dbo.Comment
    add constraint AK_Comment_PersonID_PerformanceID_Content_Datetime unique
    (
        PersonID,
		PerformanceID,
		Content,
        [Datetime]
    )
go