use performance
go

--AK_Language_Name
if OBJECT_ID ('AK_Language_Name', 'UQ') is not null
    alter table [dbo].[Language] drop constraint AK_Language_Name
go

alter table [dbo].[Language]
    add constraint AK_Language_Name unique
    (
        [Name]
    )
go
