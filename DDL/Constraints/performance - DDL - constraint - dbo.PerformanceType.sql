use performance
go

--AK_PerformanceType_Type
if OBJECT_ID ('AK_PerformanceType_Type', 'UQ') is not null
    alter table dbo.PerformanceType drop constraint AK_PerformanceType_Type
go

alter table dbo.PerformanceType
    add constraint AK_PerformanceType_Type unique
    (
        [Type]
    )
go
