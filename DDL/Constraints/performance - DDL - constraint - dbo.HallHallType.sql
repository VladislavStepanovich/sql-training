use performance
go

--FK_HallHallType_Hall
if OBJECT_ID('FK_HallHallType_Hall', 'F') is not null
    alter table dbo.HallHallType drop constraint FK_HallHallType_Hall
go

alter table dbo.HallHallType
    add constraint FK_HallHallType_Hall foreign key (HallID) references dbo.Hall (HallID)
go

--FK_HallHallType_HallType
if OBJECT_ID('FK_HallHallType_HallType', 'F') is not null
    alter table dbo.HallHallType drop constraint FK_HallHallType_HallType
go

alter table dbo.HallHallType
    add constraint FK_HallHallType_HallType foreign key (HallTypeID) references dbo.HallType (HallTypeID)
go
