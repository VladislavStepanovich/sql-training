use performance
go

--AK_Seat_Place_Row
if OBJECT_ID ('AK_Seat_HallID_Place_Row', 'UQ') is not null
    alter table dbo.Seat drop constraint AK_Seat_HallID_Place_Row
go

alter table dbo.Seat
    add constraint AK_Seat_HallID_Place_Row unique
    (
		HallID,
        Place,
        [Row]
    )
go

--FK_Seat_Hall
if OBJECT_ID('FK_Seat_Hall', 'F') is not null
    alter table dbo.Seat drop constraint FK_Seat_Hall
go

alter table dbo.Seat
    add constraint FK_Seat_Hall foreign key (HallID) references dbo.Hall (HallID)
go