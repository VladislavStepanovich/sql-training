use performance
go

--FK_Purchase_Ticket
if OBJECT_ID('FK_Purchase_Ticket', 'F') is not null
    alter table dbo.Purchase drop constraint FK_Purchase_Ticket
go

alter table dbo.Purchase
    add constraint FK_Purchase_Ticket foreign key (TicketID) references dbo.Ticket (TicketID)
go

--FK_Purchase_Person
if OBJECT_ID('FK_Purchase_Person', 'F') is not null
    alter table dbo.Purchase drop constraint FK_Purchase_Person
go

alter table dbo.Purchase
    add constraint FK_Purchase_Person foreign key (PersonID) references dbo.Person (PersonID)
go

--AK_Purchase_TicketID_PersonID
if OBJECT_ID ('AK_Purchase_TicketID_PersonID', 'UQ') is not null
    alter table dbo.Purchase drop constraint AK_Purchase_TicketID_PersonID
go

alter table dbo.Purchase
    add constraint AK_Purchase_TicketID_PersonID unique
    (
	    TicketID,
        PersonID
    )
go