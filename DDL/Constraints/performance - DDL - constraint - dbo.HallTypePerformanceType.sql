use performance
go

--FK_HallTypePerformanceType_HallType
if OBJECT_ID('FK_HallTypePerformanceType_HallType', 'F') is not null
    alter table dbo.HallTypePerformanceType drop constraint FK_HallTypePerformanceType_HallType
go

alter table dbo.HallTypePerformanceType
    add constraint FK_HallTypePerformanceType_HallType foreign key (HallTypeID) references dbo.HallType (HallTypeID)
go

--FK_HallTypePerformanceType_PerformanceType
if OBJECT_ID('FK_HallTypePerformanceType_PerformanceType', 'F') is not null
    alter table dbo.HallTypePerformanceType drop constraint FK_HallTypePerformanceType_PerformanceType
go

alter table dbo.HallTypePerformanceType
    add constraint FK_HallTypePerformanceType_PerformanceType foreign key (PerformanceTypeID) references dbo.PerformanceType (PerformanceTypeID)
go