use performance
go

--AK_Performance_PerformanceTypeID_LanguageID_Title
if OBJECT_ID ('AK_Performance_PerformanceTypeID_LanguageID_Title', 'UQ') is not null
    alter table dbo.Performance drop constraint AK_Performance_PerformanceTypeID_LanguageID_Title
go

alter table dbo.Performance
    add constraint AK_Performance_PerformanceTypeID_LanguageID_Title unique
    (
	    PerformanceTypeID,
	    LanguageID,
        Title
    )
go

--FK_Performance_PerformanceType
if OBJECT_ID('FK_Performance_PerformanceType', 'F') is not null
    alter table dbo.Performance drop constraint FK_Performance_PerformanceType
go

alter table dbo.Performance
    add constraint FK_Performance_PerformanceType foreign key (PerformanceTypeID) references dbo.PerformanceType (PerformanceTypeID)
go

--FK_Performance_Language
if OBJECT_ID('FK_Performance_Language', 'F') is not null
    alter table dbo.Performance drop constraint FK_Performance_Language
go

alter table dbo.Performance
    add constraint FK_Performance_Language foreign key (LanguageID) references [dbo].[Language] (LanguageID)
go