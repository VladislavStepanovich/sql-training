use performance
go

if OBJECT_ID('dbo.Ticket', 'U') is not null
   drop table dbo.Ticket

create table dbo.Ticket
(
    TicketID         int      not null    identity,
    SeatID           int      not null,
    PerformanceID    int      not null,
    SessionID        int      not null,
    [Date]           date     not null,
    Price            money    not null

    constraint PK_Ticket primary key (
        TicketID
    ) 
)
go