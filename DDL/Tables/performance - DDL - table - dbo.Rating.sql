use performance
go

if OBJECT_ID('dbo.Rating', 'U') is not null
   drop table dbo.Rating

create table dbo.Rating
(
    RatingID         int      not null    identity,
    PersonID         int      not null,
    PerformanceID    int      not null,
	Rate             float    not null

    constraint PK_Rating primary key (
        RatingID
    ) 
)
go