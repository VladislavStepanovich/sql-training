use performance
go

if OBJECT_ID('dbo.HallHallType', 'U') is not null
   drop table dbo.HallHallType

create table dbo.HallHallType
(
    HallID        int    not null,
    HallTypeID    int    not null

    constraint PK_HallHallType primary key (
        HallID,
		HallTypeID
    ) 
)
go