use performance
go

if OBJECT_ID('dbo.PerformanceParticipantType', 'U') is not null
   drop table dbo.PerformanceParticipantType

create table dbo.PerformanceParticipantType
(
    ParticipantTypeID    int    not null,
    ParticipantID        int    not null,
    PerformanceID        int    not null

    constraint PK_PerformanceParticipantType primary key (
        ParticipantTypeID,
		ParticipantID,
		PerformanceID
    ) 
)
go