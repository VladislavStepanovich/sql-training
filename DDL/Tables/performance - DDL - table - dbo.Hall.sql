use performance
go

if OBJECT_ID('dbo.Hall', 'U') is not null
   drop table dbo.Hall

create table dbo.Hall
(
    HallID              int             not null    identity,
    TargetBuildingID    int             not null,
    [Name]              nvarchar(20)    not null,
    Capacity            int             not null

    constraint PK_Hall primary key (
        HallID
    ) 
)
go