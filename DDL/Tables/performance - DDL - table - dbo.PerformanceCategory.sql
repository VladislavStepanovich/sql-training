use performance
go

if OBJECT_ID('dbo.PerformanceCategory', 'U') is not null
   drop table dbo.PerformanceCategory

create table dbo.PerformanceCategory
(
    CategoryID       int    not null,
    PerformanceID    int    not null

    constraint PK_PerformanceCategory primary key (
        CategoryID,
		PerformanceID
    ) 
)
go