use performance
go

if OBJECT_ID('dbo.Participant', 'U') is not null
   drop table dbo.Participant

create table dbo.Participant
(
    ParticipantID    int    not null    identity,
    PersonID         int    not null,

    constraint PK_Participant primary key (
        ParticipantID
    ) 
)
go