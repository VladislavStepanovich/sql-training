use performance
go

if OBJECT_ID('dbo.Comment', 'U') is not null
   drop table dbo.Comment

create table dbo.Comment
(
    CommentID        int              not null    identity,
    PersonID         int              not null,
    PerformanceID    int              not null,
    [Datetime]       datetime         not null,
	Content          nvarchar(500)    not null

    constraint PK_Comment primary key (
        CommentID
    ) 
)
go