use performance
go

if OBJECT_ID('dbo.HallType', 'U') is not null
   drop table dbo.HallType

create table dbo.HallType
(
    HallTypeID    int             not null    identity,
    [Type]        nvarchar(50)    not null

    constraint PK_HallType primary key (
        HallTypeID
    ) 
)
go