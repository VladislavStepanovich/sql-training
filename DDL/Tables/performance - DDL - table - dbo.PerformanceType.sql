use performance
go

if OBJECT_ID('dbo.PerformanceType', 'U') is not null
   drop table dbo.PerformanceType

create table dbo.PerformanceType
(
    PerformanceTypeID    int             not null    identity,
    [Type]               nvarchar(20)    not null,

    constraint PK_PerformanceType primary key (
        PerformanceTypeID
    ) 
)
go