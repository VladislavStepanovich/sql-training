use performance
go

if OBJECT_ID('dbo.TargetBuilding', 'U') is not null
   drop table dbo.TargetBuilding

create table dbo.TargetBuilding
(
    TargetBuildingID    int             not null    identity,
    [Name]              nvarchar(20)    not null,
    City                nvarchar(20)    not null,
    Street              nvarchar(30)    not null,
    BuildingNumber      nvarchar(10)    not null

    constraint PK_TargetBuilding primary key (
        TargetBuildingID
    ) 
)
go