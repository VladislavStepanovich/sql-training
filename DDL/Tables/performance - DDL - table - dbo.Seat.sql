use performance
go

if OBJECT_ID('dbo.Seat', 'U') is not null
   drop table dbo.Seat

create table dbo.Seat
(
    SeatID    int         not null    identity,
    HallID    int         not null,
    Place     smallint    not null,
    [Row]     smallint    null

    constraint PK_Seat primary key (
        SeatID
    ) 
)
go