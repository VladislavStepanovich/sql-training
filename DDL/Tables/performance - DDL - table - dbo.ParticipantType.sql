use performance
go

if OBJECT_ID('dbo.ParticipantType', 'U') is not null
   drop table dbo.ParticipantType

create table dbo.ParticipantType
(
    ParticipantTypeID    int             not null    identity,
    [Type]               nvarchar(20)    not null,

    constraint PK_ParticipantType primary key (
        ParticipantTypeID
    ) 
)
go