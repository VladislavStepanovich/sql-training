use performance
go

if OBJECT_ID('dbo.Person', 'U') is not null
   drop table dbo.Person

create table dbo.Person
(
    PersonID      int             not null    identity,
    FirstName     nvarchar(50)    not null,
    LastName      nvarchar(50)    not null,
    MiddleName    nvarchar(50)    null,
    Birthday      date            not null,
    [Login]       nvarchar(30)    null,
    [Password]    nvarchar(30)    null,

    constraint PK_Person primary key (
        PersonID
    ) 
)
go