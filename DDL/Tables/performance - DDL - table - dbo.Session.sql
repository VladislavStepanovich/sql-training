use performance
go

if OBJECT_ID('dbo.[Session]', 'U') is not null
   drop table dbo.[Session]

create table dbo.[Session]
(
    SessionID    int     not null    identity,
    HallID       int     not null,
    [Time]       time    not null

    constraint PK_Session primary key (
        SessionID
    ) 
)
go