use performance
go

if OBJECT_ID('dbo.HallTypePerformanceType', 'U') is not null
   drop table dbo.HallTypePerformanceType

create table dbo.HallTypePerformanceType
(
    HallTypeID           int    not null,
    PerformanceTypeID    int    not null

    constraint PK_HallTypePerformanceType primary key (
        HallTypeID,
		PerformanceTypeID
    ) 
)
go