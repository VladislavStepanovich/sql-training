use performance
go

if OBJECT_ID('dbo.Performance', 'U') is not null
   drop table dbo.Performance

create table dbo.Performance
(
    PerformanceID        int               not null    identity,
    PerformanceTypeID    int               not null,
	LanguageID           int               not null,
    Title                nvarchar(50)      not null,
    Duration             time(3)           not null,
    [Description]        nvarchar(2000)    not null,

    constraint PK_Performance primary key (
        PerformanceID
    ) 
)
go