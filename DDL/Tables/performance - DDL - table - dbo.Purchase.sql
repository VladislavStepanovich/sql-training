use performance
go

if OBJECT_ID('dbo.Purchase', 'U') is not null
   drop table dbo.Purchase

create table dbo.Purchase
(
    PurchaseID    int    not null    identity,
    TicketID      int    not null,
    PersonID      int    not null

    constraint PK_Purchase primary key (
        PurchaseID
    ) 
)
go