use performance
go

if OBJECT_ID('dbo.[Language]', 'U') is not null
   drop table dbo.[Language]

create table dbo.[Language]
(
    LanguageID    int             not null    identity,
    [Name]        nvarchar(20)    not null,

    constraint PK_Language primary key (
        LanguageID
    ) 
)
go