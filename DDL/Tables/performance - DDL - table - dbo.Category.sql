use performance
go

if OBJECT_ID('dbo.Category', 'U') is not null
   drop table dbo.Category

create table dbo.Category
(
    CategoryID    int             not null    identity,
    [Name]        nvarchar(30)    not null,

    constraint PK_Category primary key (
        CategoryID
    ) 
)
go