use performance
go

exec dbo.usp_ImportPerformanceData @parameters = N'
[{
        "title": "Maroon 5 concert",
        "duration": "03:00:00",
        "description": "Awesome group want to present their new album!",
        "performanceType": "Concert",
        "category": [
            { "name": "Pop" },
            { "name": "Rock" }
        ],
        "language": "english",
        "participants": [{
                "firstName": "Adam",
                "lastName": "Levin",
                "birthday": "1979-03-18",
                "participantType": [
                    { "type": "Singer" }
                ]
            },
            {
                "firstName": "Jessee",
                "lastName": "Carmichael",
                "birthday": "1979-04-02",
                "participantType": [
                    { "type": "Keyboardist" }
                ]
            }
        ]
    },
    {
        "title": "Rammstein concert",
        "duration": "02:00:00",
        "description": "Straight from German! Fantastic group! Amazing program!",
        "performanceType": "Concert",
        "category": [
            { "name": "Metall" },
            { "name": "Rock" }
        ],
        "language": "deutch",
        "participants": [{
                "firstName": "Till",
                "lastName": "Lindemann",
                "birthday": "1963-01-04",
                "participantType": [
                    { "type": "Singer" }
                ]
            },
            {
                "firstName": "Christian",
                "lastName": "Lorenz",
                "birthday": "1966-11-16",
                "participantType": [
                    { "type": "Guitarist" }
                ]
            }
        ]
    },
    {
        "title": "Avengers",
        "duration": "02:10:00",
        "description": "What is power for you, bro?",
        "performanceType": "Movie",
        "category": [
            { "name": "Action" },
            { "name": "Fantasy" }
        ],
        "language": "russian",
        "participants": [{
                "firstName": "Robert",
                "lastName": "Downey",
                "middleName": "Jr.",
                "birthday": "1965-04-04",
                "participantType": [
                    { "type": "Actor" }
                ]
            },
            {
                "firstName": "Scarlett",
                "lastName": "Johansson",
                "birthday": "1984-11-22",
                "participantType": [
                    { "type": "Actor" }
                ]
            },
            {
                "firstName": "Tom",
                "lastName": "Holland",
                "birthday": "1996-06-01",
                "participantType": [
                    { "type": "Actor" }
                ]
            },
            {
                "firstName": "Joseph",
                "lastName": "Hill",
                "birthday": "1964-06-23",
                "participantType": [
                    { "type": "Director" }
                ]
            }
        ]
    },
    {
        "title": "Shrek",
        "duration": "01:40:00",
        "description": "Its Big!",
        "performanceType": "Movie",
        "category": [
            { "name": "Action" },
            { "name": "Fantasy" },
            { "name": "Comedy" },
            { "name": "Family" },
            { "name": "Adventure" },
            { "name": "Cartoon" }
        ],
        "language": "english",
        "participants": [{
                "firstName": "Mike",
                "lastName": "Myers",
                "birthday": "1963-05-25",
                "participantType": [
                    { "type": "Actor" }
                ]
            },
            {
                "firstName": "Eddie",
                "lastName": "Murphi",
                "birthday": "1961-04-03",
                "participantType": [
                    { "type": "Actor" }
                ]
            },
            {
                "firstName": "Andrew",
                "lastName": "Adamson",
                "birthday": "1966-12-01",
                "participantType": [
                    { "type": "Director" }
                ]
            }
        ]
    },
    {
        "title": "Nutcraker and four kingdoms",
        "duration": "02:50:00",
        "description": "Its time to let cat out of the bag!",
        "performanceType": "Theater",
        "category": [
            { "name": "Fantasy" },
            { "name": "Family" },
            { "name": "Adventure" },
            { "name": "Fairytale" },
            { "name": "Drama" },
            { "name": "Ballet" }
        ],
        "language": "russian",
        "participants": [{
                "firstName": "Lasse",
                "lastName": "Hallstrom",
                "birthday": "1946-06-02",
                "participantType": [
                    { "type": "Director" }
                ]
            },
            {
                "firstName": "Linus",
                "lastName": "Sandgren",
                "birthday": "1972-12-05",
                "participantType": [
                    { "type": "Cameraman" }
                ]
            }
        ]
    }
]'