use performance
go

exec dbo.usp_ImportPurchasesData @parameters = N' 
  [{
         "Ticket": {
             "performance": "Shrek",
             "language": "english",
             "performanceType": "Movie",
             "building": "October",
             "city": "Minsk",
             "hall": "Small hall",
             "date": "2020-03-08",
             "time": "19:00:00",
             "seat": {
                 "row": 1,
                 "place": 5
             },
             "price": 7
         },
         "Person": {
             "firstName": "Tatiana",
             "lastName": "Zueva",
             "middleName": "Alexandrovna",
             "birthday": "2000-01-24"
         }
     },
     {
         "Ticket": {
             "performance": "Shrek",
             "language": "english",
             "performanceType": "Movie",
             "building": "October",
             "city": "Minsk",
             "hall": "Small hall",
             "date": "2020-03-08",
             "time": "19:00:00",
             "seat": {
                 "row": 1,
                 "place": 6
             },
             "price": 7
         },
         "Person": {
             "firstName": "Polina",
             "lastName": "Metla",
             "middleName": "Georgievna",
             "birthday": "2000-06-12"
         }
     },
     {
         "Ticket": {
             "performance": "Shrek",
             "language": "english",
             "performanceType": "Movie",
             "building": "October",
             "city": "Minsk",
             "hall": "Small hall",
             "date": "2020-03-08",
             "time": "19:00:00",
             "seat": {
                 "row": 1,
                 "place": 10
             },
             "price": 5
         },
         "Person": {
             "firstName": "Vlad",
             "lastName": "Stepanovich",
             "middleName": "Ostapovich",
             "birthday": "2000-06-11"
         }
     }
 ]'