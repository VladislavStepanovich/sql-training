use performance;
go

exec dbo.usp_ImportPersonData @parameters = N'
[
    {
        "firstName": "Tatiana",
        "lastName": "Zueva",
        "middleName": "Alexandrovna",
        "login": "tatianazueva",
        "password": "123",
        "birthday": "2000-01-24"
    }
    ,
    {
        "firstName": "Polina",
        "lastName": "Metla",
        "middleName": "Georgievna",
        "login": "polinaMetla",
        "password": "123456",
        "birthday": "2000-06-12"
    }
    ,
    {
        "firstName": "Vlad",
        "lastName": "Stepanovich",
        "middleName": "Ostapovich",
        "login": "vladStepanovich",
        "password": "1234567",
        "birthday": "2000-06-11"
    }
]'