use performance 
go

exec dbo.usp_ImportBuildingsData @parameters = N'
[{
        "name": "October",
        "address": {
            "city": "Minsk",
            "street": "prospect Nezavisemosti",
            "buildingNumber": "56"
        },
        "halls": [{
                "name": "Small hall",
                "capacity": 40,
                "type": "Odeum",
                "seats": [{
                        "row": 1,
                        "places": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    },
                    {
                        "row": 2,
                        "places": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    },
                    {
                        "row": 3,
                        "places": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    },
                    {
                        "row": null,
                        "places": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    }
                ],
                "sessions": [{
                        "time": "11:00:00"
                    },
                    {
                        "time": "14:00:00"
                    },
                    {
                        "time": "16:00:00"
                    },
                    {
                        "time": "19:00:00"
                    }
                ],
                "availablePerformanceTypes": [{
                    "type": "Movie"
                }]
            },
            {
                "name": "Large hall",
                "capacity": 60,
                "type": "Odeum",
                "seats": [{
                        "row": 1,
                        "places": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    },
                    {
                        "row": 2,
                        "places": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    },
                    {
                        "row": 3,
                        "places": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    },
                    {
                        "row": 4,
                        "places": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    },
                    {
                        "row": 5,
                        "places": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    },
                    {
                        "row": 6,
                        "places": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    }
                ],
                "sessions": [{
                        "time": "11:00:00"
                    },
                    {
                        "time": "14:00:00"
                    },
                    {
                        "time": "16:00:00"
                    },
                    {
                        "time": "19:00:00"
                    }
                ],
                "availablePerformanceTypes": [{
                        "type": "Movie"
                    },
                    {
                        "type": "Theater"
                    }
                ]
            }
        ]
    },
    {
        "name": "Prime-center",
        "address": {
            "city": "Minsk",
            "street": "prospect Pobeditelei",
            "buildingNumber": "96"
        },
        "halls": [{
            "name": "Prime-hall",
            "capacity": 10,
            "seats": [{
                "row": null,
                "places": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            }],
            "type": "Concert hall",
            "sessions": [{
                "time": "20:00:00"
            }],
            "availablePerformanceTypes": [{
                "type": "Concert"
            }]
        }]
    }
]'