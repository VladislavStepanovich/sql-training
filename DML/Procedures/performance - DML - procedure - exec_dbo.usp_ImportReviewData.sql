use performance
go

exec dbo.usp_ImportReviewData @parameters = N'
[{
        "performance": "Shrek",
        "language": "english",
        "performanceType": "Movie",
        "rating": 10.0,
        "comment": "Amazing cartoon! Want to rewatch!",
        "person": {
            "firstName": "Tatiana",
            "lastName": "Zueva",
            "middleName": "Alexandrovna",
            "birthday": "2000-01-24"
        },
        "datetime": "2020-03-08 23:00:15.000"
    },
    {
        "performance": "Shrek",
        "language": "english",
        "performanceType": "Movie",
        "rating": 9.9,
        "comment": "Cool!",
        "person": {
            "firstName": "Polina",
            "lastName": "Metla",
            "middleName": "Georgievna",
            "birthday": "2000-06-12"
        },
        "datetime": "2020-04-02 10:05:20.000"
    },
    {
        "performance": "Shrek",
        "language": "english",
        "performanceType": "Movie",
        "comment": "Cartoon with soul...",
        "person": {
            "firstName": "Vlad",
            "lastName": "Stepanovich",
            "middleName": "Ostapovich",
            "birthday": "2000-06-11"
        },
        "datetime": "2020-12-23 18:10:46.000"
    }
]'